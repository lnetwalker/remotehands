#!/bin/bash

# testrunner.sh
# runs all tests in this directory
# copyright (c) 2023 by Hartmut Eilers <hartmut@eilers.net>
# released under the terms of

check_result () {
    res=$1
    if [ "$res" != "0" ]; then
        exit $res
    fi
}

# get script name ( whoami )
# later used to skip self execution in case
# scriptname matches testfile names
myself=$0

# check debug mode 
# debug gets 1, when script 
# is started with "bash".
# if started with "bash -x"
# debug will be greater 1
debug=$(ps a|grep -c "bash -x $myself")

# get all tests and execute them
TESTSCRIPTS=$(ls test*.sh)

# loop over all tests and execute them
for curr_test in $TESTSCRIPTS; do
    # but skip myself
    if [ "./$curr_test" != "$myself" ]; then
        # run test in debug mode ?
        if [ "$debug" == "1" ]; then
            # no, standard mode
            bash $curr_test
            check_result $? 
        else
            # yes, debug mode
            bash -x $curr_test
            check_result $?
        fi
    fi
done
