#!/bin/bash

# copyright (c) 2023 by Hartmut Eilers <hartmut@eilers.net>
# released under the terms of

# testheader
echo "------------------------------------------------"
echo "test2.sh - test function getOwnIp ()"
echo

# source the lib
. ../lib/remotehandslib.sh
if [ "$?" != "0" ]; then
    exit 2
fi

# test the function of getOwnIp ()
IP="$(getOwnIp)"
echo $IP
exit 0

