#!/bin/bash

# copyright (c) 2023 by Hartmut Eilers <hartmut@eilers.net>
# released under the terms of

# testheader
echo "------------------------------------------------"
echo "test1.sh - test function getStdGw ()"
echo

# source the lib 
. ../lib/remotehandslib.sh
if [ "$?" != "0" ]; then
    exit 2
fi

# test the function of getStdGw ()
IP="$(getStdGw)"
echo $IP
exit 0
