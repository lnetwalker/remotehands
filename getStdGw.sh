#!/bin/bash

# getStdGw.sh

# little shell snippet that display the configured
# standard gw of the network.

# copyright (c) 2023 by Hartmut Eilers <hartmut@eilers.net>
# released under the terms of
# source the lib 
. lib/remotehandslib.sh
if [ "$?" != "0" ]; then
    exit 2
fi

echo $(getStdGw)
