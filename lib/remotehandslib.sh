#!/bin/bash

# remotehandslib.sh provides helping functions
# for the base commandset of remotehands
# copyright (c) 2023 by Hartmut Eilers <hartmut@eilers.net>
# released under the terms of

# catch any error in the lib and return error
set -e
trap 'catch $? $LINENO' EXIT

catch() {
    if [ "$1" != "0" ]; then
        # error handling goes here
        echo "Error $1 occurred on $2"
        exit 1
    fi
}

getStdGw () {
    # returns the standard gateway
    STD_GW=`ip route |grep default| cut -d " " -f3|tail -n1`
    echo "$STD_GW"
}

getOwnIp () {
    # returns the IPv4 adresses of the computer
    OWN_IP=""
    # get interesting part of ip addr command
    FILE=$(ip addr|grep "inet "| grep -v "virbr")
    # read it line by line and extract the IP address
    while IFS= read LINE ;do
        # collect IP addresses in OWN_IP
        OWN_IP="$OWN_IP $(echo $LINE|cut -d " " -f2)"
    done < <(printf '%s\n' "$FILE")
    # print the result
    echo "$OWN_IP"
}
